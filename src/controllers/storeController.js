const headers =  {'Content-Type': 'application/json'};
export async function getProductList(){
    const storeurl = process.env.REACT_APP_STORE_URL;
    const response = await fetch(storeurl, {headers: headers});
    return response;
}

export async function updateStoreData(store_id, adding){
    let url = process.env.REACT_APP_STORE_URL;
    url += adding? "decrease/1/" : "increase/1/";
    url += store_id;
    const response = await fetch(url, 
            {
                method: 'PUT',
                headers: headers
            });
    return response;
}