import React , {useState} from 'react';
import Header from './components/header';
import ProductList from './components/ProductList';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import cartBehaviour from './behaviour/CartBehaviour';
import detectWindoVisibility from './windowVisibility/windowVisibilityDetector'

// global variables
var cartTree = {};
var {hidden, visibilityChange} = detectWindoVisibility();

function App() {
  
  const [cartTotalValue, setCartTotalValue] = useState(0);


  let cartCount = (storeId, action)=>{
    let actionType = action? "ADD" : "REMOVE";
    let c = cartTotalValue;
    if(action){
      c++;
    }
    else{
      c--;
    }
    cartTree = cartBehaviour(storeId, actionType, cartTree);
    setCartTotalValue(c);
  }


  let updateCart = (storeId, quantity)=>{
    let currentAmount = 0;
    if(cartTree){
      if(cartTree[storeId]){
        currentAmount = cartTree[storeId];
      }
    }
    let difference = quantity - currentAmount ;
    let actionType = difference > 0? "ADD" : "REMOVE";
    
    cartTree = cartBehaviour(storeId, actionType, cartTree, Math.abs(difference));

    let c = cartTotalValue+difference;
    setCartTotalValue(c);
  }


  return (
      <div className="App">
        <header className="App-header">
          <Header totalItemsInCart = {cartTotalValue} />
        </header>
          <div className="app-content">
            <ProductList  
              handleCartCount = {(id, action) => cartCount(id, action)}
              handleUpdateCart = {(id, amount) => updateCart(id, amount)}
              cartTree={cartTree}
              hidden = {hidden}
              visibilityChange = {visibilityChange}
            />
          </div>
      </div>
  );
}

export default App;
