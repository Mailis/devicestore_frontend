import React from 'react';

function Cart(props){
    
    return(
        <div>
            <span>Total in cart: </span>
            <span className="quantity_box">{props.cartAmount}</span>
            <span> items</span>
        </div>
    );
}

export default Cart;