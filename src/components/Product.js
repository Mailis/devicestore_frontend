import React from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

function Product (props){

    return(
            <div onClick={props.handleShowModal}>
                <Row className="product_title">
                    <Col>
                        {props.title}
                    </Col>
                    <Col>
                        <span className="quantity_box" >{props.quantity}</span>
                    </Col>
                </Row>
            </div>
    );
}

export default Product;