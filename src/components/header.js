import React from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Cart from './cart';

function Header(props){
   
    return (
        <Row>
            <Col className="col-lg appname">Test Application</Col>
            <Col className="col-md-auto">
                <Cart
                    cartAmount = {props.totalItemsInCart}
                />
            </Col>
            
        </Row>
    );
    
}


export default Header;