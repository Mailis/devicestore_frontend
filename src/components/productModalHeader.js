import React from 'react';
import Col from 'react-bootstrap/Col';

function ProductModalHeader(props){
    return(
        <>
            <Col className="col-lg">
            <h3>{props.header}</h3>
            </Col>
            <Col className="col-sm-4">
                <h3>{props.color}</h3>
            </Col>
            <Col className="col-sm-auto">
                <span  className="quantity_box">{props.quantity}</span>
            </Col>
        </>
    );
}

export default ProductModalHeader;