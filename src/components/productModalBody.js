import React from 'react';
import Col from 'react-bootstrap/Col';
import AddRemoveButton from './buttons/addRemoveButton';

function ProductModalBody(props){
    const quantity = props.quantity;
    let addingBtnClass = (quantity<1? "btn-secondary disabled" : "btn-success" );
    const item_cart_amount = props.item_cart_amount;
    let removingBtnClass = (item_cart_amount<1? "btn-secondary disabled" : "btn-warning" );

    return(
        <>
            <AddRemoveButton
                className= {"btn modal-btn " + addingBtnClass}
                header ={"add to cart"}
                handleClick = {props.handleAddToCart}
                disabled={quantity<1}
            />
            <br />
            <AddRemoveButton
                className= {"btn modal-btn " + removingBtnClass}
                header ={"remove from cart"}
                handleClick = {props.handleRemoveFromCart}
                disabled={item_cart_amount<1}
            />
            <div className = "modal-btn">
                    <span>
                        {props.bodyText} 
                    </span>
                    <span className="five_px_left">
                        {item_cart_amount}
                    </span>
            </div>
        </>
    );
}

export default ProductModalBody;