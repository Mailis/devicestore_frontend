import React from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import CloseButton from './buttons/closeButton';
import CloseCrossButton from './buttons/closeCrossButton';


function Modal(props){
    return(
        <div className="modal-content">
            <div className="modal-header">
                {props.modalHeader}

                <Col className="col-sm-auto">
                    <CloseCrossButton 
                        handleClose = {props.handleClose}
                    />
                </Col>
            </div>
            <Row className="modal-body">
                
                <Col>
                    {props.description}
                </Col>
                <Col>
                    {props.modaBlody}
                    
                    <CloseButton
                        handleClose = {props.handleClose}
                    />

                </Col>
            </Row>
            <div className="modal-footer">
            </div>
        </div>
    )
}

export default Modal;