import React from 'react';
import Container from 'react-bootstrap/Container';
import {getProductList, updateStoreData} from '../controllers/storeController';
import Modal from './Modal';
import Product from './Product';
import ProductModalHeader from './productModalHeader';
import ProductModalBody from './productModalBody';


class ProductList extends React.Component{
    
    constructor(props) {
        super(props);
        this.state = {
            products: null,
            modalData: null,
            modalIsVisible: false,
            hidden: props.hidden,
            visibilityChange : props.visibilityChange,
            noticeList : null
        }
    }

     componentDidMount = async() => {
       const response = await getProductList();
       const products = await response.json();
       //populate products list
       if(products){
            this.setState({products}, function(){
                this.updateInitialLocalStorageOnStateChange();
            });

        }
      
        //listeners for cases when window is hidden by browser tab switching
        const hidden = this.state.hidden;
        const visibilityChange = this.state.visibilityChange;
        
        if (typeof document.addEventListener === "undefined" || hidden === undefined) {
            console.log("This demo requires a browser, such as Google Chrome or Firefox, that supports the Page Visibility API.");
        } 
        else {
            // Handle page visibility change
            document.addEventListener(visibilityChange, this.handleVisibilityChange, false);
            // cleanup this component
            return () => {
                document.removeEventListener(visibilityChange, this.handleVisibilityChange);
            };
        }
    }

    handleVisibilityChange = () => {
        if(!document[this.state.hidden]) {
          this.checkForDataUpdate();
        }
    }

    checkForDataUpdate = () => {
        let prodlist = this.state.products;
        let noticeList = [];
        //check for dta changes and
        //update data if changes occurred
        prodlist.forEach(p => {
            let storedItem = JSON.parse(localStorage.getItem(p._id));
            let storedItemQ = storedItem[0];
            let quantityOnCart = storedItem[1];
            if(p.quantity !== storedItemQ){
               let notice = p.product.title;
                notice += p.quantity > storedItemQ? " decreased by " : " increased by ";
                notice += Math.abs(p.quantity - storedItemQ);
                noticeList.push(notice);
                p.quantity = storedItemQ;
                
                this.props.handleUpdateCart(p._id, quantityOnCart);
            }
        });

        //notice user about changed data
        if(noticeList.length > 0){
            this.setState(
                {
                    noticeList, 
                    modalIsVisible: true
                });
        }   
    }
      

    showModal = (modalData) =>{   
        this.setState({modalData});
        this.setState({modalIsVisible : true});
    }

    hideModal = () =>{
        this.setState({modalData : null});
        this.setState({noticeList : null});
        this.setState({modalIsVisible : false});
    }

    updateInitialLocalStorageOnStateChange = () => {
        let prodlist = this.state.products;
        prodlist.forEach(p => {
            let itemCountOnCart = 0;
            //check if in any case something stored in cart from browser localstorage
            let itemStoredData = localStorage.getItem(p.id);
            if(itemStoredData)
                itemCountOnCart = JSON.parse(itemStoredData)[1];
            localStorage.setItem(p._id, JSON.stringify([p.quantity, itemCountOnCart]));
        });
    }


    modifyCart = async (store_id, adding = true) =>{
        const response = await updateStoreData(store_id, adding);
        const r = await response.json();
        
        if(r.ok){
            let prodlist = [...this.state.products];
            let modData = {...this.state.modalData};
            prodlist.forEach(p => {
                if(p._id === store_id){
                    let q = p.quantity;
                    let quantityOnCart = this.getItemAmountFromCart(store_id) || 0;
                    if(adding){
                        q--;
                        quantityOnCart++;
                    }
                    else{
                        q++;
                        quantityOnCart--;
                    }
                    p.quantity = q;
                    modData.quantity = q;
                    localStorage.setItem(p._id, JSON.stringify([q, quantityOnCart]));
                }
            });
            this.setState({products : prodlist, modalData : modData});
            this.props.handleCartCount(store_id, adding);
        }
        return r.ok;
    }

    addToCart = async (store_id) =>{
        await this.modifyCart(store_id);
    }

    removeFromCart = async (store_id) =>{
        await this.modifyCart(store_id, false);
    }


    getItemAmountFromCart(storeId){
        let cartTreeBranchAmount = undefined;
        if(this.props.cartTree){
            cartTreeBranchAmount = this.props.cartTree[storeId];
        }
        return cartTreeBranchAmount;
    }

    displayNoticeChanges = () =>{
        let notices = [...this.state.noticeList];
        let noticeArray = null;
        if(notices){
            noticeArray = [];
            notices.forEach((n, i) => {
                noticeArray.push(
                    <div key = {n+i}>
                        {n}
                    </div>
                )
            })
        }
        return noticeArray;
    }
    
    buildNoticeModal = () =>{
        let noticeArray = this.displayNoticeChanges();
        let noticeHeader = "Someone changed the data:";
        return(
            <Modal
                modalHeader={noticeHeader}
                description = {noticeArray}
                modaBlody={""}
                handleClose = {this.hideModal}
            />
        );
    }


    buildProductModal = () =>{
        let data = this.state.modalData;
        let storeId=data.store_id
        let cartTreeBranchAmount = this.getItemAmountFromCart(storeId);
        
        if(data){
            return(
                <Modal
                    modalHeader={
                        <ProductModalHeader 
                            header = {data.header}
                            color = {data.color}
                            quantity = {data.quantity}/>
                    }
                    modaBlody={
                        <ProductModalBody 
                            description = {data.description}
                            handleAddToCart = {() => this.addToCart(data.store_id)}
                            handleRemoveFromCart = {() => this.removeFromCart(data.store_id)}
                            quantity = {data.quantity}
                            item_cart_amount = {cartTreeBranchAmount || 0}
                            bodyText = {"this item(s) in cart: "}/>
                      }
                    handleClose = {this.hideModal}
                    description = {data.description}
                />
            );
        }
        return null;
    }


    displayProdList = () => {
        const prodList = this.state.products;
        const container = [];
        if(prodList){
            prodList.forEach((storeitem, index) => {
                if(storeitem){
                    if(storeitem.product){
                        const modalData = {};
                        modalData.store_id = storeitem._id;
                        modalData.header =  storeitem.product.title;
                        modalData.quantity =  storeitem.quantity;
                        modalData.description = storeitem.product.description;
                        modalData.color = storeitem.product.color.color;
                        modalData.cart_amount = 33;
                        container.push(
                            <Container key = {"prod" + index}>
                                <Product
                                    handleShowModal = {() => this.showModal(modalData)}
                                    title = {storeitem.product.title}
                                    quantity = {storeitem.quantity}
                                />
                            </Container>
                        );
                    }
                }
            });
        }
        return container;
    }

    render(){
        const prodList = this.state.products? this.displayProdList() : "Loading...";
        const modalClassname = this.state.modalIsVisible? "shown" : "hidden";
        let modalWindow = null;
        if(this.state.noticeList && this.state.noticeList.length > 0){
            modalWindow = this.buildNoticeModal();
        }
        else if(this.state.modalData){
            modalWindow = this.buildProductModal();
        }
        
        return (
            <div>
                <div className = { modalClassname}>
                    <div className="modalwindow">
                        {modalWindow}
                    </div>
                </div>
                <div className="prod_list">
                    {prodList}
                </div>
            </div>
        );
    }
}

export default ProductList;