function CloseButton(props){
    return(
        <button type="button" 
                className="btn btn-info modal-btn"  
                onClick={props.handleClose}>
            close
        </button>
    );
}

export default CloseButton;