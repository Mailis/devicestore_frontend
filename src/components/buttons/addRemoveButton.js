function AddRemoveButton(props){
    return(
        <button type="button" 
                className= {props.className}
                onClick={props.handleClick}
                disabled={props.disabled}>
            {props.header}
        </button>
    );
}

export default AddRemoveButton;